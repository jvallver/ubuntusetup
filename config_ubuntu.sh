#!/bin/bash

#Ubuntu set up script
#Author: Jordi Vallverdu [jvallver@gmail.com]

function installQuestion {
	read -p "install $1 [y/n]? "
}

installQuestion "vim"
if [ "$REPLY" == "y" ]; then
	echo "installing vim ..."
	apt-get install vim 
fi

read -p "configure vi? [y/n]? "
if [ "$REPLY" == "y" ]; then
	echo "configuring vi ..."
	echo -e "syntax on\nset nu\nset ts 4\n" > ~/.vimrc
fi

installQuestion "java"
if [ "$REPLY" == "y" ]; then
	echo "installing java ..."
	add-apt-repository ppa:webupd8team/java
	apt-get update
	mkdir -p /usr/lib/mozilla/plugins
	apt-get install oracle-jdk7-installer
fi

installQuestion "eclipse"
if [ "$REPLY" == "y" ]; then
	echo "installing eclipse ..."
	read -p "Enter eclipse download url: "
	wget -O "eclipse.tar.gz" "$REPLY"
	tar xzf "eclipse.tar.gz"
	mv eclipse /opt/
	chown -R root:root /opt/eclipse
	chmod -R +r /opt/eclipse
	echo -e '#!/bin/sh\nexport ECLIPSE_HOME="/opt/eclipse"\n$ECLIPSE_HOME/eclipse $*\n' > /usr/bin/eclipse
	chmod 755 /usr/bin/eclipse
    echo -e '[Desktop Entry]\nEncoding=UTF-8\nName=Eclipse\nComment=Eclipse IDE\nExec=eclipse\nIcon=/opt/eclipse/icon.xpm\nTerminal=false\nType=Application\nCategories=GNOME;Application;Development;\nStartupNotify=true' > /usr/share/applications/eclipse.desktop
fi

installQuestion "dropbox"
if [ "$REPLY" == "y" ]; then
	echo "installing dropbox ..."
	cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xz
	~/.dropbox-dist/dropboxd
fi

installQuestion "spotify"
if [ "$REPLY" == "y" ]; then
	echo "installing spotify ..."
	echo 'deb http://repository.spotify.com stable non-free' >> /etc/apt/sources.list
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 94558F59
	apt-get update
	apt-get install spotify-client-qt 
fi

installQuestion "terminator"
if [ "$REPLY" == "y" ]; then
    echo "installing terminator ..."
    apt-get install terminator
fi

installQuestion "gparted"
if [ "$REPLY" == "y" ]; then
	echo "installing gparted ..."
	apt-get install gparted
fi

installQuestion "git"
if [ "$REPLY" == "y" ]; then
	echo "installing git"
	apt-get install git
	read -p "enter your git email: "
	git config --global user.mail "$REPLY"
	read -p "enter your git username: "
	git config --global user.name "$REPLY"
fi

installQuestion "python-pip"
if [ "$REPLY" == "y" ]; then
	echo "installing pip ..."
	apt-get install python-pip
fi

installQuestion "htop"
if [ "$REPLY" == "y" ]; then
    echo "installing htop ..."
    apt-get install htop
fi

installQuestion "ant"
if [ "$REPLY" == "y" ]; then
	echo "installing ant ..."
	apt-get install ant
fi
